<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('tinker', function () {
    return Auth::user();
});



Auth::routes();

Route::post('ideas/{idea}/comment', 'IdeasController@storeComment')->name('ideas.comment.store');

Route::post('ideas/{idea}/vote', 'IdeasController@storeVote')->name('ideas.vote.store');

Route::resources([
    'ideas' => 'IdeasController',
    'comments'=> 'CommentsController'
]);

Route::get('', 'HomeController@index')->name('home');

Route::get('users', 'UserController@index')->name('showUsers');

Route::get('guestIdea', 'GuestController@showGuest')->name('guest');





