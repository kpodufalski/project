<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    @yield('styles')


    <style>
        html, body {
            background-color: #fff;
            color: #4a5256;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .create {
            position: fixed;
            bottom: 50px;
            right: 100px;
        }
        .navbar{background: darkgreen;}
        .nav-item::after{content:'';display:block;width:0px;height:2px;background:#fec400!important;transition: 0.2s!important;}
        .nav-item:hover::after{width:100%;}
        .navbar-dark .navbar-nav .active > .nav-link, .navbar-dark .navbar-nav .nav-link.active, .navbar-dark .navbar-nav .nav-link.show, .navbar-dark .navbar-nav .show > .nav-link,.navbar-dark .navbar-nav .nav-link:focus, .navbar-dark .navbar-nav .nav-link:hover{color:#fec400!important;}
        .nav-link{padding:15px 5px;transition:0.2s!important;}
        .dropdown-item.active, .dropdown-item:active{color:#212529!important;}
        .dropdown-item:focus, .dropdown-item:hover{background:#fec400!important;}

        .logoo{
            position: relative;
            right: 80px;
            top: 1px;

        }
        .white{
           color: #ffffff;
        }
        </style>

</head>
<body>
    <div id="app" >
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <a href="/" class="navbar-brand logoo"><img src="../svg/idea.svg" alt="idealogo"><code>IdeAll</code></a>
                <div class="row">
                <div class="collapse navbar-collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="{{route('ideas.index')}}" class="nav-link white" >Ideas</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('showUsers')}}" class="nav-link white" >Users</a>
                        </li>
                        <li class="col-md-8 offset-md-2">
                            <h6><code class="white">Don’t worry about people stealing your ideas. If your ideas are any good, you’ll have to ram them down people’s throats.
                                    – Howard Aiken</code></h6>
                        </li>
                    </ul>
                </div>
                </div>
                    <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto white">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            @if (Route::has('register'))
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown white">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right dropdown " aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </div>

@section('create')
        <div class="create" >
            <div class="row justify-content-center">
            <a href="{{route('ideas.create')}}"><img src="../svg/create.png" ></a>
                <br>
                <br>
                <br>
            </div>
            <div class="row">
                <a href="{{route('ideas.create')}}">
                    <button type="button" class="btn btn-success">Create an Idea !</button>
                </a>
            </div>
        </div>
   @show
        <div>
            @section('content')

            @show
        </div>


    {{-- Include JS dependendencies --}}
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.1/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    @section('js')
    @show
</body>
</html>
