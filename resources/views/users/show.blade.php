@extends('layouts.app')

@section('content')

    <h1>Showing {{ $user->name }}</h1>
    <p>
        {{ $user->email }}
    </p>

@endsection