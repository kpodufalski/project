@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/cards.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .bg {
            background-image: url("../svg/wall.jpg");
            background-size: cover;

        }
    </style>
@endsection

@section('content')
<div class="bg">
    <div class="container">
        <h1 style="text-align: center;">All Users</h1>
            <div class="row">
                @foreach ($users as $user)
                    <div class="col-md-4">
                        <div class="card">
                                <div class="box">
                                <h5 class="card-title">{{ $user->name }}</h5>
                                <p class="card-text">{{ $user->email }}</p>
                            </div>
                        </div>

                        <br>
                    </div>
                @endforeach
            </div>
    </div>
</div>
@endsection