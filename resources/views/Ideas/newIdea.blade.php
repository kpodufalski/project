

@extends('layouts.app')
<style>
    .bg {
        background-image: url("../svg/write.jpg");
        background-size: cover;

    }

    #backwhite{
        background-color: rgba(0, 100, 0, 0.75);
        color: #1b1e21;
        border-radius: 20px;
    }
    .textarea {
        resize: vertical;
        height: 40px;
        min-height: 40px;
        max-height:100px;
        font-size: 20px !important;
    }
</style>
@section('content')
    <div class="bg">
        <div class="container">
            <br>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card" id="backwhite">
                        <div class="card-header"><code style="color: #fec400">Submit your idea!</code></div>

                        <div class="card-body">
                            <form action='{{ route('ideas.store') }}' method="post">
                                @csrf
                                <br>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Title</span>
                                    </div>
                                    <textarea class="form-control form-control-sm textarea" name="title" aria-label="With textarea" required autofocus></textarea>
                                </div>

                                <br>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Content</span>
                                    </div>
                                    <textarea class="form-control textarea" name="content" aria-label="With textarea" required autofocus></textarea>
                                </div>
                                <br>

                                <div class="input-group">
                                    <select class="custom-select" id="inputGroupSelect04" name="aspect" aria-label="aspect">
                                        <option selected>Aspect of business...</option>
                                        <option value="Construction">Construction</option>
                                        <option value="Management">Management</option>
                                        <option value="Health, Safety, and Environment">Health, Safety, and Environment</option>
                                        <option value="Technology">Technology</option>
                                        <option value="HR and Recruitment">HR and Recruitment</option>
                                        <option value="Office management">Office management</option>
                                        <option value="other">Other...</option>


                                    </select>
                                    <div class="input-group-append">

                                        <button class="btn btn-outline-secondary" type="Submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
@endsection
@section('create')
@endsection