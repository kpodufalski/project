

@extends('layouts.app')

    <style>
        .bg {
            background-image: url("../backBulb.jpeg");
            background-repeat: no-repeat;
            background-size: cover;
        }

        .backwhite{
            background-color: rgba(255,255,255, 0.85);
            color: #1b1e21;
        }
        .comment {
            padding: 0 5px;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 20px;
            box-shadow: 0px 0px 12px rgba(0,0,0,0.9);
        }
        .ideaBox {
            padding: 0 5px;
            border: 1px solid #000;
            border-radius: 5px;
            margin-bottom: 20px;
            box-shadow: 0px 0px 12px rgba(255,255,0,0.9);
        }
        .commentForm {
            border-radius: 2px;
            box-shadow: 0px 0px 12px rgba(0,0,0,0.9);
        }
        .boxx {
            border-radius: 150px;
            box-shadow: 0px 0px 12px rgba(0,0,0,0.9);
        }

    </style>
@section('content')

    <div class="bg">
        <div class="container">
            <br>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="backwhite ideaBox">
                            <h1 style="text-align: center">{{$idea->title}}</h1>
                            <hr>
                            <h2>{{$idea->content}}</h2>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center"  style="font-family: Times; color: darkgreen;">
                <div class="card backwhite boxx ">

                        <h5>Created by:
                            <span class="badge badge-pill badge-dark">{{$idea->user->name}}</span>
                        </h5>

                </div>
            </div>
            <br>
            <div class="row justify-content-center" style="font-family: Times; color: darkgreen;">
                <div class="card backwhite boxx ">

                        <h5>Aspect of business:
                            <span class="badge badge-pill badge-dark"> {{$idea->aspect  }}</span>
                        </h5>

                </div>
            </div>
            <br>
            <div class="row justify-content-center">
                <form action="#" method="get" class="vote-widget" >
                    <button :disabled="vote" type="button" name="up" class="btn btn-success" @click="upvote()">
                        Upvote
                        <span class="badge badge-light">@{{ up }}</span>
                        <span class="sr-only">Upvote</span>
                    </button>

                    <button :disabled="vote" type="button" name="down" class="btn btn-secondary" @click="downvote()">
                        Downvote
                        <span class="badge badge-light">@{{ down }}</span>
                        <span class="sr-only">Downvote</span>
                    </button>
                    <p style="color: #040505" v-if="vote">You @{{ vote.up ? 'Up' : 'Down' }}voted this!</p>
                </form>
            </div>
            <br>
            <div align="center" style="font-family: Times; color: yellow;">
                <h1>Comments</h1>
            </div>

            @include('comments.index', ['comments' => $idea->comments])


            <div class="row justify-content-center ">
                <div class="card backwhite ">


                    <div class="card-body commentForm">

                        <form action='{{ route('ideas.comment.store', $idea->id) }}' method="post">


                            @csrf


                            <div class="input-group mb-3">
                                <input type="text" name="content" class="form-control" placeholder="Write a comment..." aria-label="Recipient's username" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="submit" name="GO!" id="button-addon2">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @if ($idea->user_id == Auth::id())
                <form action="{{route('ideas.destroy', $idea->id)}}" method="post">
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger">Delete the Idea</button>
                </form>
            @endif
            <br><br><br><br><br><br><br><br><br>

        </div>
    </div>



@endsection

@section('js')
    <script>
        var voteUrl = '{{ route('ideas.vote.store', $idea->id) }}';
        var upVotes = {{ $idea->getNumUpVotes() }};
        var downVotes = {{ $idea->getNumDownVotes() }};
        var vote = {!! $vote ? $vote : 'null' !!};
        new Vue({
            el: '.vote-widget',
            data: {
                up: upVotes,
                down: downVotes,
                vote: vote
            },
            methods: {
                upvote: function () {
                    this.doVote(true);
                },
                downvote: function () {
                    this.doVote(false);
                },
                doVote: function (up) {
                    var that = this;
                    var params = {
                        up: up,
                        _token: '{{ csrf_token() }}'
                    };
                    $.post(voteUrl, params, function () {

                    }).fail(function () {
                        alert('failed!!');
                        that.vote = null;

                        if (up)
                            that.up--;
                        else
                            that.down--;
                    });
                    this.vote = {up: up};
                    if (up)
                        this.up++;
                    else
                        this.down++;
                }
            }
        });

    </script>
@endsection