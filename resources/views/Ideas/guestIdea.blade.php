@extends('layouts.app')
<style>
    #bg {
        background-image: url("../backBulb.jpeg");
        height: 100%;

    }

    #backwhite{
        background-color: rgba(201,215,2, 0.7);
        color: #1b1e21;
        border-radius: 4px;
        box-shadow: 0px 0px 12px rgba(0,205,0,0.9);
    }
    .textarea {
        resize: vertical;
        height: 40px;
        min-height: 40px;
        max-height:100px;
        font-size: 20px !important;
    }
</style>
    <div id="bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <br><br><br>
                    <div class="card" id="backwhite">
                        <div class="card-header">Submit your idea !!!</div>

                        <div class="card-body">
                            <form action='#' method="post">
                                @csrf
                                <br>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Title</span>
                                    </div>
                                    <textarea class="form-control form-control-sm textarea" name="title" aria-label="With textarea" required autofocus></textarea>
                                </div>

                                <br>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Content</span>
                                    </div>
                                    <textarea class="form-control textarea" name="content" aria-label="With textarea" required autofocus></textarea>
                                </div>
                                <br>
                                <br>
                                <div class="input-group">
                                    <select class="custom-select" id="inputGroupSelect04" name="aspect" aria-label="aspect">
                                        <option selected>Aspect of business...</option>
                                        <option value="Planning">Planning</option>
                                        <option value="Management">Management</option>
                                        <option value="Health, Safety, and Environment">Health, Safety, and Environment</option>
                                        <option value="Principles of Technology">Principles of Technology</option>
                                        <option value="Labor Issues">Labor Issues</option>
                                        <option value="Personal Work Habits">Personal Work Habits</option>
                                        <option value="other">Other...</option>


                                    </select>
                                    <div class="input-group-append">

                                        <button class="btn btn-outline-secondary" type="Submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
