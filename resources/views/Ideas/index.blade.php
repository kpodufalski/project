@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/cards.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
    .bg {
        background-image: url("../svg/wall.jpg");
        background-size: cover;
    }
    </style>
@endsection


@section('content')
    <div class="bg">
    <div class="container">
    <h1 style="text-align: center;">All Ideas</h1>
    <div class="row">
  @foreach ($Ideas as $idea)

          <div class="col-md-6">
            <div class="jumbotron">
                <h1 class="display-4">{{ $idea->title }}</h1>
                <hr class="my-4">
                <p>{{ $idea->content }}</p>
                <a class="btn btn-primary btn-lg" href="/ideas/{{ $idea->id }}" role="button">See the Idea !</a>
            </div>
          </div>

 @endforeach
    </div>
@endsection


    </div>
    </div>