@foreach($comments as $comment)
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="backwhite comment">
            <h2>{{ $comment->content }}
            </h2>
            <h4 align="right">By: {{$comment->user->name}}</h4>

        </div>
    </div>
</div>
@endforeach
