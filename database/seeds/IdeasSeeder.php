<?php

use App\Idea;
use App\User;
use Illuminate\Database\Seeder;

class IdeasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ideas')->truncate();

        $idea = new Idea();
        $idea->title ='Example title';
        $idea->content ="nice content ! ";
        $idea->implement=false;
        $idea->aspect="sales";
        $idea->user_id = User::first()->id;
        $idea->save();

        $idea2 = Idea::create([
            'title' => 'free lunch',
            'content' => " for ALLL !",
            'implement'=>false,
            'aspect'=>"managment",
            'user_id'=> User::first()->id
        ]);
    }
}
