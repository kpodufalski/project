<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate(); // !!!!!

        $user = new User();
        $user->email ='kpodufalski@alacrityfoundation.com';
        $user->name ="Kirol";
        $user->password = Hash::make('password123');
        $user->save();

        $user2 = User::create([
            'email' => 'jacob@alacrityfoundation.com',
            'name' => "Jacob",
            'password' => Hash::make('password321')
        ]);
        $user3 = User::create([
            'email' => 'karol@mail.com',
            'name' => "Karol",
            'password' => Hash::make('123456')
        ]);

    }
}
