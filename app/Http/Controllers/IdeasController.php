<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Idea;
use App\Http\Controllers\Controller;
use App\User;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IdeasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Ideas = Idea::all(); //$abc = Idea::findOrFail(0);
        return view('Ideas.index', compact('Ideas'));

    }

    public function store(Request $req)
    {
        $params = $req->only('title', 'content', 'aspect');
        $newIdea = Auth::user()->ideas()->create($params);

        return redirect()->route('ideas.show', $newIdea->id);
    }

    protected function create()
    {
        return view('Ideas/newIdea');
    }
//    public function show(Idea $idea)
//    {
//        $Idea = Idea::all();
//        return view('Idea.index', compact('idea'));
//    }
    public function show(Idea $idea )
    {
        $vote = $idea->votes()->whereUserId(Auth::id())->first();
        return view('Ideas.singleIdea', compact('idea', 'vote'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Idea $idea)
    {
        $idea->delete();

        return redirect()->route('ideas.index');

    }

    public function storeComment(Request $request, Idea $idea)
    {
        $idea->comments()->create([
            'content' => $request->input('content'),
            'user_id' => Auth::id(),

        ]);
        return redirect('/ideas/' . $idea->id);
    }
    public function storeVote(Request $request, Idea $idea)
    {
        // turn the vote into true or false
        $up = filter_var($request->input('up'), FILTER_VALIDATE_BOOLEAN);
        return $idea->votes()->create([
            'up' => $up,
            'user_id' => Auth::id()
        ]);
    }



}

