<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use App\Idea;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function index()
    {
        $comm = Comment::all();
        return view('comments.index', compact('comm'));
    }


    public function create()
    {
        //
        return view('comments.create');
    }


    public function store(Request $request)
    {

//        $comment = new Comment();
//        $comment->content = $request->content;
//        $comment->user()->insertGetId();
//        $comment->idea();
//        $comment->save();
//
//        return redirect('#');
    }



    public function show(Comment $comment)
    {
        return view('Ideas.singleIdea', compact('comment'));
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
