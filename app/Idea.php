<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    use Notifiable;

    protected $fillable = [
        'title', 'content', 'implement', 'aspect'
    ];

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    public function getNumUpVotes()
    {
        return $this->votes()->whereUp(true)->count();//naathan?
    }

    public function getNumDownVotes()
    {
        return $this->votes()->whereUp(false)->count();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

